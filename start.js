// Get dependencies.
var _ = require('underscore');
var fs = require('fs');
var chalk = require('chalk');

var inquirer = require('inquirer');
var git = require('simple-git')('./');

JiraApi = require('jira').JiraApi;

// Path Files.
var versionMDPath  = 'assets/version.md',
    versionMD = fs.readFileSync(versionMDPath, "utf8");

// Tanımlamalar.
var pkg = versionMD.split('\n')[1].split(' ')[2].replace(/(\r\n|\n|\r)/gm, ""),
    issueID = versionMD.split('\n')[2].split(': ')[1],
    vFix = versionMD.split('\n')[3].split(': ')[1];




var sorgu = [
    {
        type: "confirm",
        name: "jira",
        message: "JIRA da bir Issue kapatılacak mı?"
    },
    {
        type: "confirm",
        name: "commit",
        message: "Commit Olarak Gidecek mi?"
    },
    {
        type: "list",
        name: "repo",
        message: "Origin üzerinde repon hangisi olacak?",
        choices: [
            {name: "master"},
            {name: "develop"}
        ],
        default : "develop",
        when: function (answers) {
            return answers.commit;
        }
    },
    {
        type: "input",
        name: "username",
        message: "JIRA Username ?",
        when: function (answers) {
            return answers.jira;
        }
    },
    {
        type: "password",
        name: "password",
        message: "JIRA Şifre ?",
        when: function (answers) {
            return answers.jira;
        }
    },
    {
        type: "input",
        name: "issue",
        message: "Issue ID >>  " + issueID,
        default: issueID
    },
    {
        type: 'input',
        name: 'comment',
        message: 'Commit ve/veya JIRA Yorumu yazınız'
    },
    {
        type: "list",
        name: "action",
        message: "JIRA'da Yapacağınız İşlem Nedir ?",
        choices: [
            {name: "Comment"},
            {name: "Resolve"}
        ],
        when: function (answers) {
            return answers.jira;
        }
    },
    {
        type: "list",
        name: "resolution",
        message: "Issue Resolution?",
        choices: [
            {name: "Fixed"},
            {name: "Wont Fix"},
            {name: "Duplicate"},
            {name: "Incomplete"},
            {name: "Cannot Reproduce"},
            {name: "Done"}
        ],
        when: function (answers) {
            return answers.action;
        }
    }
];

inquirer.prompt(sorgu).then(function (answers) {
    // Jira işlemlerinde seçili gelen parametreye göre ayrı ayrı ID tanımlaması yapılmak zorunda..
    switch (answers.action) {
        case 'Resolve':
            var actionID = 5;
            break;
        default:
    }


    var result = answers,
        jira = new JiraApi('https', 'jira.dogannet.tv', '', result.username, result.password, '2'),
        resolveResponse =  { "update": {"comment": [ { "add": {"body": "UXQuark v" + pkg + "\n\n" + result.comment + "\n\n Fixed: " + vFix} }]},"fields": {"resolution": {"name": result.resolution}}, "transition": { "id": actionID }};

    if(result.jira == true){
        if(result.action == 'Comment'){
            jira.addComment(answers.issue, answers.comment, function(error, success){
                if(success == 'Success') {
                    console.log(chalk.red.bold(answers.issue) + ' Numaralı Issue detayına ' + chalk.white.underline.bold(answers.comment) + ' mesajınız eklenmiştir.');
                } else {
                    console.log(chalk.white.bgRed.bold('addComment Bir HATA oluştu!'));
                }
            });
        } else {
            jira.transitionIssue(result.issue, resolveResponse, function(error, response){
                if(response == 'Success') {
                    console.log('Info >>>> Issue: ' + chalk.red.underline.bold(answers.issue) + ' Comment: ' + chalk.white.underline.bold(answers.comment) + ' Result: ' + chalk.white.underline.bold(answers.action) + ' Resolution: ' + chalk.white.underline.bold(answers.resolution));
                } else {
                    console.log(chalk.white.bgRed.bold('transitionIssue Bir HATA oluştu!'));
                }
            });
        }
    }

    if(result.commit == true){

        //Eğer bir jira issue'si girilmiş ise commit mesajına ilave edilecek. Girilmemiş ise sadece Comment görünecek şeklinde gönderilecek.
        if(result.jira){
            var jiraCommit = result.issue + ' #' + result.resolution + ' (' + result.comment + ')';
        }else{
            var jiraCommit = result.comment;
        }
        git.init().then(function() {
            console.log(chalk.red('Lütfen Bekleyin!.'));
        }).add('./*').then(function() {
            console.log(chalk.green('Tüm Dosyalar Indexlendi'));
        }).push('origin', result.repo).then(function() {
            console.log('Dosyalar ' + chalk.cyan(result.repo) + ' reposuna push edildi.');
        }).commit(jiraCommit).then(function() {
            console.log('Dosyalar ' + chalk.cyan(result.repo) + ' reposuna ' + chalk.cyan(jiraCommit) + ' mesajı ile commit edildi');
        });
    }
});

/*
 {name: "Stop Progress"},
 {name: "Resolved Close"}



 content-type : application/json
 X-AREQUESTID    : 873x904460x1
 X-ASESSIONID    : 1k36v2x
 X-AUSERNAME     : erhan.erdogan
 X-Content-Type-Options  : nosniff
 Cookie  : __gfp_64b=kymFlmL7Sa7kDgdn.xEWRinbBnAFmhaLvlgQPXU9BCf.57; JSESSIONID=1B204193B2F70102B5C6B13599BC4C7D; atlassian.xsrf.token=B1G1-52UC-5H4F-KA6H|aea70c5620ef26d4dc34987e3de1f145a7e7a4fd|lin; AJS.conglomerate.cookie="|hipchat.inapp.links.first.clicked.erhan.erdogan=false"; __utmt_tempo_planner=1; __utmt_navlinks=1; __utma=1.1940220726.1462620647.1462620647.1462620647.1; __utmb=1.3.9.1462620656245; __utmc=1; __utmz=1.1462620647.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)
 X-Seraph-LoginReason: Ok


 {
 type: 'input',
 message: 'Resolve edilmemiş. Bilgin olsun',
 name: 'dsadasd',
 when: function (answers) {
 if(answers.action === 'Close'){
 var id = answers.issue;
 jira.listTransitions(id, function(error, response){
 var id = _.pluck(response.transitions, 'id');
 var bla = _.contains(id, '5');
 });

 return bla;
 }
 }
 },
 
if(vJira == 'true'){
    if(action == 'Resolve'){
         jira.transitionIssue(vIssue, resolveResponse, function(error, response){
            console.log(response);
         });
        jira.listTransitions(vIssue, function(error, response){
            var listing = response
        });
    }
}

*/