/**
 * @author Doğan TV Software Section
 */

var QuarkJS = QuarkJS || {};

(function(app) {
    'use strict';

    app.init = function() {
        app.runPlugins();
        setTimeout(function() {
            app.Version.init();
        });
    };

    app.runPlugins = function() {
        app.classieSwipe();
        app.highlightJs();
        app.loadPage();
        app.wrapperHeight();
    };

    /* jshint ignore:start */
    app.highlightJs = function() {
        //hljs.initHighlightingOnLoad();
        Prism.highlightAll();
    };

    app.classieSwipe = function() {
        var menuLeft = document.querySelector('.documents-menu'),
            menuRight = document.querySelector('.documents-simulation'),
            showLeftPush = document.querySelector('.showLeftPush'),
            showRightPush = document.querySelector('.showRightPush'),
            containerClassie = document.querySelector('.documents-container');

        showLeftPush.onclick = function() {
            classie.toggle( this, 'active' );
            classie.toggle( containerClassie, 'content-to-right' );
            classie.toggle( menuLeft, 'content-open' );
            disableOther('showLeftPush');
        };

        showRightPush.onclick = function() {
            classie.toggle( this, 'active' );
            classie.toggle( containerClassie, 'content-to-left' );
            classie.toggle( menuRight, 'content-open' );
            disableOther( 'showRightPush' );
        };

        function disableOther(button) {
            if(button !== 'showLeftPush' ) {
                classie.toggle( showLeftPush, 'disabled' );
            }
            if(button !== 'showRightPush' ) {
                classie.toggle( showRightPush, 'disabled' );
            }
        }
    };

    app.loadPage = function() {
        var newHash     = '',
            $mainContent = $('.documents-container');

        //Sayfa yüklenirken hash kontrol edilir var ise o sayfa direk yüklenir.
        window.onload = function() {
            if (window.location.hash.length > 1) {
                var hashURL = window.location.hash.split("#")[1];
                $mainContent.load(hashURL);
            }
        };

        $('body').delegate('.load-page', 'click', function() {
            window.location.hash = $(this).data('href');
            return false;
        });

        $(window).bind('hashchange', function() {
            newHash = window.location.hash.substr(1);
            $mainContent.load(newHash);
        });

        // Loading div animation
        $(document).ajaxStart(function(){
            $(".pageLoading").css("display","block");
        });

        $(document).ajaxComplete(function(){
            setTimeout(function(){
                $(document).ready(function() {
                    Prism.highlightAll();
                });
            }, 0);

            setTimeout(function(){
                app.classieSwipe();
                UXQuarkJS.bindPlugins();
                $(".pageLoading").css("display","none");
            }, 1000);
        });

        setTimeout(function(){
            app.classieSwipe();
            $(".pageLoading").css("display","none");
        }, 1000);
    };
    /* jshint ignore:end */

    app.wrapperHeight = function(){
        var wrapHeight = $('#wrapper').height();
        var windowHeight = window.innerHeight;

        if(wrapHeight < windowHeight){
            $('#wrapper, .documents-content, .documents-menu').css('height', windowHeight - 41);
        }
    };
})(QuarkJS);

$(QuarkJS.init);