/**
 * @author Doğan TV Software Section
 */

var QuarkJS = QuarkJS || {};

(function(app) {
    'use strict';

    var _this = {
        versions  : {},
        stylesheet: 'uxquark.min.css',

        init    : function() {
            var sheets = document.styleSheets;

            for(var i = 0; i < sheets.length; i++) {
                if(sheets[i].href && sheets[i].href.indexOf(_this.stylesheet) !== -1) {
                    _this.styles = sheets[i];
                }
            }

            _this.script();
            _this.style();
        },
        script  : function() {
            _this.versions.script = app.version;
        },
        style   : function() {
            if(_this.styles.cssRules !== null) {
                _this.versions.style = _this.styles.cssRules[_this.styles.cssRules.length - 1].style.content.replace(/"/g, '');
            }
            else {
                _this.versions.style = null;
            }
        },
        get     : function(type) {
            if(Object.keys(_this.versions).length < 2) {
                _this.init();
            }

            return _this.versions[type] || _this.versions;
        },
        show    : function() {
            console.info('%cUXQuark:', 'font-weight: bold', 'Application scripts version ' + _this.get('script') + ' and styles version ' + _this.get('style'));
        },
        isSynced: function() {
            if(_this.get('script') !== _this.get('style') || _this.get('script') !== _this.get('lib')) {
                console.warn('%cUXQuark:', 'font-weight:bold', 'Your local copy of application scripts (v. ' + _this.get('script') + ') and application styles (v. ' + _this.get('style') + ') are not in sync. Please update your static contents.');
            }
            else {
                console.info('%cUXQuark:', 'font-weight:bold', 'Your lib, scripts and styles are in synced');
            }
        }
    };

    app.Version = _this;
}(QuarkJS));