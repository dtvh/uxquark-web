/**
 * @author Doğan TV Software Section
 */

var UXQuarkJS = UXQuarkJS || {};

(function(app) {
    'use strict';

    app.initialized = false;

    app.selectors = {
        body:         'body'
    };

    app.classes = {};
    app.$objs = {};
    app.vals = {};

    app.init = function() {
        if(app.initialized) {
            console.warn('%cUXQuarkJS', 'font-weight: bold', 'is allready initialized');
            return;
        }
        app.$objs.body      = $(app.selectors.body);

        app.runPlugins();

        /* Tools Start */
        app.form.init();
        /* Tools End */

        app.initialized = true;
    };

    app.runPlugins = function() {
        app.bindPlugins();
    };
    app.bindPlugins = function() {
        Object.keys(app.config.uxQuarkPlugins).map(function(plugin) {
            if(typeof $.fn[plugin] === 'function') {
                if(_.isArray(app.config.uxQuarkPlugins[plugin])) {
                    _.forEach(app.config.uxQuarkPlugins[plugin], function(n) {
                        var options = n.options || {};
                        $(n.selector)[plugin](options);
                    });
                }
                else {
                    var options = app.config.uxQuarkPlugins[plugin].options || {};
                    $(app.config.uxQuarkPlugins[plugin].selector)[plugin](options);
                }
            }
        });
    };

})(UXQuarkJS);

$(UXQuarkJS.init);