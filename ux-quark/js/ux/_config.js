var UXQuarkJS = UXQuarkJS || {};

(function(app) {
    'use strict';

    var config = {};

    config.uxQuarkPlugins = {
        collapsible:     {
            selector: '.collapsible',
            options:  {
                closeSiblings: true,
                current:       'current'
            }
        },
        dialog:          {
            selector: '.dialog',
            options:  {
                close: false
            }
        },
        modal:           {
            selector: '.modal',
            options:  {
                fixed:        true,
                maxWidth:     '90%',
                maxHeight:    '90%',
                appendTo:     '#form',
                overlayClose: false
            }
        },
        tabs:            {
            selector: '.ux-tabs'
        },
        tooltip:         {
            selector: '.tooltip'
        }
    };

    config.filter = {
        onKeys:    true,
        onReturn:  true,
        minLength: 2,
        wait:      650,
        empty:     false
    };

    app.config = config;
})(UXQuarkJS);