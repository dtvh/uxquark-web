/**
 * UX Tabs
 */

;(function($) {
    var ux,

        defaults = {
            tabNavigation       : '.tab-nav',
            tabContentWrapper   : '.tab-content',
            onReady             : false
        };

    // constructor method
    var Tabs = function(el, options){
        var opts = $.extend({}, defaults, options);

        callback(opts.onReady);

        $(opts.tabNavigation).on('click', 'a', function(event){
            event.preventDefault();

            var selectedItem = $(this);
            var offSetParentNav = selectedItem.offsetParent().find(opts.tabNavigation);
            var offSetParentWrapper = selectedItem.offsetParent().find(opts.tabContentWrapper);

            if(!selectedItem.hasClass('current') ) {
                var selectedTab = selectedItem.data('content'),
                    selectedContent = offSetParentWrapper.find('li[data-content="'+selectedTab+'"]'),
                    slectedContentHeight = selectedContent.innerHeight();

                offSetParentNav.find('a.current').removeClass('current');

                selectedItem.addClass('current');
                selectedContent.addClass('active').siblings('li').removeClass('active');

                offSetParentWrapper.animate({
                    'height': slectedContentHeight
                }, 1);
            }
        });
    };

    // global callback
    var callback = function(fn) {
        // if callback string is function call it directly
        if(typeof fn === 'function') {
            fn.apply(this);
        }

        // if callback defined via data-attribute, call it via new Function
        else {
            if(fn !== false) {
                var func = function() {
                    return fn;
                };
                func();
            }
        }
    };

    // jquery bindings
    ux = $.fn.tabs = $.uxtabs = function(options){
        return this.each(function(){
            var $el, tabs;
            $el = $(this);

            tabs = new Tabs($el, options);
        });
    };

    // Version
    ux.version = '1.0.0';

    // settings
    ux.settings = defaults;

})(jQuery);