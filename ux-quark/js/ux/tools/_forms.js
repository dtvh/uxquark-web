var UXQuarkJS = UXQuarkJS || {};

(function(app) {
    'use strict';

    app.form = {};

    app.form.classNames = {
        isActive:  'is-active',
        isBlurred: 'is-blurred'
    };

    app.form.selectors = {
        textFields:      '.is-text-field input, .is-text-field textarea',
        radioFields:     '.is-radio input',
        checkFields:     '.is-checkbox input',
        preventedFields: '[data-submit="false"]'
    };

    app.form.events = {
        click:    'click.formEvents',
        focus:    'focus.formEvents',
        blur:     'blur.formEvents',
        keyup:    'keyup.formEvents',
        keypress: 'keypress.formEvents',
        input:    'input.formEvents',
        change:   'change.formEvents'
    };

    app.form.$objs = {};

    app.form.init = function() {
        app.form.label();
        app.form.readonly();
        app.form.preventSubmit();

        document.addEventListener('touchstart', function(e) {
            if(!app.form.isInput(e.target) && app.form.isInput(document.activeElement)) {
                document.activeElement.blur();
            }

        }, false);
    };

    app.form.isInput = function(node) {
        return ['INPUT', 'TEXTAREA'].indexOf(node.nodeName) !== -1;
    };

    app.form.label = function() {
        app.$objs.body
            .on(app.form.events.focus + ' ' + app.form.events.input + ' ' + app.form.events.change, app.form.selectors.textFields, function() {
                $(this).parents('label').addClass(app.form.classNames.isActive).removeClass(app.form.classNames.isBlurred);
            })
            .on(app.form.events.blur, app.form.selectors.textFields, function() {
                if($(this).val().trim() === '' && (!$(this).attr('placeholder') || $(this).attr('placeholder') === '')) {
                    $(this).parents('label').removeClass(app.form.classNames.isActive);
                }
                else {
                    $(this).parents('label').addClass(app.form.classNames.isBlurred);
                }
            });
    };

    // fixes focus problem on readonly fields in iOS
    app.form.readonly = function() {
        app.$objs.body
            .on(app.form.events.focus, 'input[readonly]', function() {
                $(this).trigger('blur');
            });
    };

    app.form.preventSubmit = function() {
        app.$objs.body
            .on(app.form.events.keyup + ' ' + app.form.events.keypress, app.form.selectors.preventedFields, function(e) {
                var code = e.keyCode || e.which;

                if(code === 13) {
                    e.preventDefault();
                    return false;
                }
            });
    };
})(UXQuarkJS);
