// Get dependencies.
var fs = require('fs');

/**
 * Read version.md and add version info to js.
 *
 * @param: config
 */
module.exports = function(config) {
    if(typeof config !== 'object') {
        config = require('../config');
    }

    // File paths.
    var versionMDPath  = config.paths.assets + 'version.md',
        versionJSPath  = config.paths.app    + '_version.js',
        versionCSSPath = config.paths.scss   + '_version.scss';

    // Read file.
    var versionMD = fs.readFileSync(versionMDPath, "utf8");

    // Get version info.
    var version = versionMD.split('\n')[0].split(' ')[2].replace(/(\r\n|\n|\r)/gm, "");

    // Create script that will set version info.
    var versionScript = "(window.UXQuark ? window.UXQuark.version    = '" + version + "' : window.UXQuark = {version:    '" + version + "'}); // jshint ignore:line";

    // Create scss for checking version info
    var versionCSS = '.quark-version { content: "' + version + '"; }';

    // Save version info under 'uxquark.js' at first line;
    fs.writeFileSync(versionJSPath, versionScript, 'utf8');
    fs.writeFileSync(versionCSSPath, versionCSS, 'utf8');

    // Log status.
    console.log('Version info saved. (' + version + ')');
};