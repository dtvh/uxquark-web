/**
 * @author Doğan TV Software Section
 */

module.exports = {
    build  : {
        description: 'Default build tasks.',
        tasks      : ['sass', 'jshint', 'concat', 'uglify', 'notify']
    },
    default: {
        description: 'Default tasks',
        tasks      : ['build', 'watch']
    }
};
