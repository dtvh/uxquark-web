/**
 * @author Doğan TV Software Section
 */

module.exports = {
    options: {
        sourceMap: false
    },
    app   : {
        src : '<%= scripts %>',
        dest: '<%= paths.scripts %><%= package.appname %>.js'
    }
};