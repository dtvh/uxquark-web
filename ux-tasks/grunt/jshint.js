/**
 * @author Doğan TV Software Section
 */

module.exports = {
    files: ['<%= paths.app %>**/*.js'],
    options: {
        jshintrc: true
    }
};