/**
 * @author Doğan TV Software Section
 */

module.exports = {
    watch: {
        options: {
            title: "Build Complete", // optional
            message: "SASS and Uglify Finished"
        }
    }
};
