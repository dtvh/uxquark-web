/**
 * @author Doğan TV Software Section
 */

module.exports = {
    app: {
        options: {
            banner: '<%= banner %>'
        },
        files: {
            '<%= paths.scripts %><%= package.appname %>.min.js': ['<%= concat.app.dest %>']
        }
    }
};