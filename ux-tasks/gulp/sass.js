/**
 * @author Doğan TV Software Section
 */

'use strict';

var gulp = require('gulp'),
    config = require('../config'),
    sourcemaps = require('gulp-sourcemaps'),
    sass = require('gulp-sass'),
    rename = require('gulp-rename'),
    notify = require('gulp-notify');

module.exports = function() {
    return gulp.src(config.paths.scss + '**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'compressed'
        })).on('error', notify.onError('Error: <%= error.message %>'))
        .pipe(rename('uxquark.min.css'))
        .pipe(sourcemaps.write('./', {includeContent: false}))
        .pipe(notify({message: 'Sass styles completed', onLast: true}))
        .pipe(gulp.dest(config.paths.styles));
};